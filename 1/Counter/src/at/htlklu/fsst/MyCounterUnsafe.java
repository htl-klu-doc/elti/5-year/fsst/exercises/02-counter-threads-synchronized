package at.htlklu.fsst;

public class MyCounterUnsafe extends MyCounter {
	private long value = 0;

	@Override
	public String toString() {
		return String.format("Counter: %05d", this.value);
	}

	public void increment() {
		value++;
	}

	public void decrement() {
		value--;
	}
}
