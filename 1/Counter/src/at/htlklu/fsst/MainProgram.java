package at.htlklu.fsst;

import java.util.ArrayList;

public class MainProgram {
	public static void main(String[] args) throws InterruptedException {
		long countTo = 10000000;
		int numOfThreads = 4;

		MyCounter counterMT = new MyCounter();
		long timeMT = multiThreaded(numOfThreads, counterMT, countTo);
		MyCounter counterMTunsafe = new MyCounterUnsafe();
		long timeMTunsafe = multiThreaded(numOfThreads, counterMTunsafe, countTo);
		MyCounter counterST = new MyCounter();
		long timeST = singleThreaded(numOfThreads, counterST, countTo);

		System.out.println("Multi-Threaded:");
		printTime(counterMT, timeMT);
		System.out.println("Multi-Threaded unsafe:");
		printTime(counterMTunsafe, timeMTunsafe);
		System.out.println("Single-Threaded:");
		printTime(counterST, timeST);
	}

	public static long multiThreaded(int numOfThreads, MyCounter counter, long countTo) throws InterruptedException {
		long begin = System.nanoTime();
		ArrayList<Thread> threads = new ArrayList<>();

		for (int i = 0; i < numOfThreads; i++) {
			Worker w = new Worker(counter, countTo, 0);
			threads.add(w);
			w.start();
		}

		for (Thread thread : threads) {
			thread.join();
		}

		long end = System.nanoTime();
		return end - begin;
	}

	public static long singleThreaded(int pseudoThreads, MyCounter counter, long countTo) throws InterruptedException {
		return multiThreaded(1, counter, countTo * pseudoThreads);
	}

	public static void printTime(MyCounter counter, long timeInNs) {
		long ns = timeInNs % 1000;
		long us = (timeInNs / (int) Math.pow(10, 3)) % 1000;
		long ms = (timeInNs / (int) Math.pow(10, 6)) % 1000;
		long s = (timeInNs / (int) Math.pow(10, 9)) % 1000;

		System.out.printf("Result: %s, Took: %03ds %03dms %03dus %03dns%n", counter, s, ms, us, ns);
	}
}
