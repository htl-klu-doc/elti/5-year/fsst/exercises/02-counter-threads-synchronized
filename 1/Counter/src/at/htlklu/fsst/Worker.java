package at.htlklu.fsst;

public class Worker extends Thread {
	private MyCounter cReference;
	private int delay = 10;
	private long limit = 1000;

	public Worker(MyCounter cReference) {
		this.setcReference(cReference);
	}

	public Worker(MyCounter cReference, long limit) {
		this(cReference);
		this.setLimit(limit);
	}

	public Worker(MyCounter cReference, long limit, int delay) {
		this(cReference, limit);
		this.setDelay(delay);
	}

	@Override
	public void run() {
		for (long l = 0; l < limit; l++) {
			cReference.increment();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public MyCounter getcReference() {
		return cReference;
	}

	public void setcReference(MyCounter cReference) {
		this.cReference = cReference;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}
}
