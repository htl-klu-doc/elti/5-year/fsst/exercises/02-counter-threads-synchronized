package at.htlklu.fsst;

public class MyCounter {
	private long value = 0;

	@Override
	public String toString() {
		return String.format("Counter: %05d", this.value);
	}

	public synchronized void increment() {
		value++;
	}

	public synchronized void decrement() {
		value--;
	}
}
